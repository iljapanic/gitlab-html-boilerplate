# gitlab-html-boilerplate

Bare minimum to get a simple website running on GitLab Pages.

Uses vanilla HTML, CSS and JavaScript.

Put everything related to the public-facing website into `/public` directory.


## Using Browsersync

Use Browserync to speed up local development. This is optional.

You need to have Node.js installed before installing Browsesync.

[Install Node.js](https://nodejs.org/en/) and then `npm install -g browser-sync`

Then just run `sh browsersync.sh` and off you code!

